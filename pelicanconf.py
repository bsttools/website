#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'RJ Heywood'
SITENAME = 'BST Tools'
SITESUBTITLE = 'A Bootstrapping toolchain for <a href="https://buildstream.build/">BuildStream</a>.'
SITEURL = 'https://bst.tools/'
SITEIMAGE = '/images/bsttools-logo.png width=140 height=140'

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

MENUITEMS = (
    ('Articles', 'articles.html'),
    ('Gitlab repos', 'https://gitlab.com/bsttools/'),
)


# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = 'theme-alchemy/alchemy'

LINKS = [
	('git &#10149;', 'https://gitlab.com/bsttools/bsttools')
]


PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

# move the articles to a 'articles' subdir
ARTICLE_PATHS = ["articles"]
ARTICLE_URL = "articles/{slug}/"
ARTICLE_SAVE_AS = "articles/{slug}/index.html"
INDEX_SAVE_AS = "articles.html"

STATIC_PATHS = [

	'images/',

]
