slug: index
title: About

BST-Tools aims to provide a bootstrapping environment, that supports cross-compile, for the next generation of [BuildStream](https://buildstream.build/) projects. It is capable of building software for a(n as yet) small, but growing variety of platforms. By adding BST-Tools to your project, you can simplify the process of building your software for all of the platforms supported by BST-Tools.

Projects leveraging BST-Tools are capable of producing Standalone Applications for typical Desktop Computers to entire Operating Systems for Embedded Devices.

## BST-Tools In Action

Here are links to a couple of demo projects using BST-Tools.

### Simple AppImage

[stm8flash.AppImage](https://gitlab.com/bsttools/stm8flash.appimage)

This demos how to build a simple application ([stm8flash](https://github.com/vdudouyt/stm8flash.git)) for Linux, and package it as an AppImage.  
stm8flash was chosen for its minimal dependencies, and because few distros package it officially.

### Buildstream AppImage

[buildstream.AppImage](https://gitlab.com/bsttools/buildstream.appimage)

This demos how to build a complicated project with BST-Tools, and package it into an AppImage: BuildStream its self.

This shows;

 * how you can target an older LibC with an up-to-date toolchain,
 * demonstrates making all the binaries produced relocatable (something important for AppImages) by configuring it globally for the whole project at buildtime,
 * demonstrates cross-compiled python,
 * and provides a means for using BST-Tools without having to install BuildStream first, by providing pre-built AppImages.

### Embedded Firmware

[initramfs sample](https://gitlab.com/bsttools/initramfs-sample)

This is a simple Linux userland and kernel using busybox.
The output of which can be tested using QEMU.

### GNU/Linux Userland

[BST-Tools/Linux From Scratch 10](https://gitlab.com/bsttools/linux-from-scratch-10)

A Full GNU/Linux userland based on [Linux From Scratch](https://linuxfromscratch.org/), with the added bonus that the end result can be cross compiled.
I'm using this repository as a test bed to add new features to the toolchain.
